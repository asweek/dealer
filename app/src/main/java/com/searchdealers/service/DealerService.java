package com.searchdealers.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.searchdealers.di.HasComponent;
import com.searchdealers.di.component.AppComponent;
import com.searchdealers.event.ErrorEvent;
import com.searchdealers.event.GetDirectionEvent;
import com.searchdealers.event.GpsNotEnabledEvent;
import com.searchdealers.event.LoadPlacesEvent;
import com.searchdealers.event.PlacesReceivedEvent;
import com.searchdealers.helper.DealersHelper;
import com.searchdealers.helper.DirectionHelper;
import com.searchdealers.util.DistanceUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.subjects.PublishSubject;

public class DealerService extends Service implements LocationListener {

    public static final String LOG_TAG = DealerService.class.getSimpleName();

    private final double RADIUS = 100000;
    private DealerBinder binder = new DealerBinder();
    private PublishSubject<Location> locationSubject;
    // flag for network status
    boolean isNetworkEnabled = false;// flag for GPS status
    boolean isGPSEnabled = false;


    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 5; // 5 sec

    // Declaring a Location Manager
    protected LocationManager locationManager;

    @Inject
    public DealersHelper helper;

    @Inject
    public DirectionHelper directionHelper;

    @Inject
    public LocationManager mLocationManager;

    @Inject
    public DistanceUtil distanceUtil;

    @Override
    public void onCreate() {
        super.onCreate();

        ((HasComponent<AppComponent>) getApplicationContext())
                .getComponent().inject(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i(LOG_TAG, "DealerService onBind()");
        return binder;
    }

    @Override
    public void onRebind(Intent intent) {
        Log.i(LOG_TAG, "DealerService onRebind()");
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(LOG_TAG, "DealerService onUnbind()");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "DealerService onDestroy()");
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if (locationSubject != null) {
            locationSubject.onCompleted();
            locationSubject = null;
        }
        stopUsingGPS();
        super.onDestroy();
    }

    @Subscribe(sticky = true)
    public void onLoadPlacesEvent(LoadPlacesEvent event) {
        EventBus.getDefault().removeStickyEvent(event);

        locationSubject = PublishSubject.create();

        getLocation();
        if (!canGetLocation()) {
            EventBus.getDefault().postSticky(new GpsNotEnabledEvent());
            return;
        }
        Observable.zip(helper.getDealers(), locationSubject.asObservable(), PlacesReceivedEvent::new)
                .flatMap(e -> {
                    Location coordinates = e.getCoordinates();
                    double latitude = coordinates.getLatitude();
                    double longitude = coordinates.getLongitude();
                    return Observable.just(e)
                            .map(e1 -> e1.getData().getDealers())
                            .flatMapIterable(e1 -> e1)
                            .filter(dialer -> {
                                double latitude1 = dialer.getLatitude();
                                double longitude1 = dialer.getLongitude();
                                double distance = distanceUtil.distVincenty(latitude1, longitude1,
                                        latitude, longitude);
                                dialer.setDistance(distance);
                                return distance <= RADIUS;
                            })
                            .toSortedList((dealer, dealer2) ->
                                    Double.compare(dealer.getDistance(), dealer2.getDistance()))
                            .flatMap(list -> {
                                e.setDealersNearMe(list);
                                return Observable.just(e);
                            });
                })
                .subscribe(e -> {
                    stopUsingGPS();
                    EventBus.getDefault().postSticky(e);
                }, throwable -> {
                    throwable.printStackTrace();
                    stopUsingGPS();
                    EventBus.getDefault().postSticky(new ErrorEvent("Error during loading places"));
                });
    }
    @Subscribe(sticky = true)
    public void onGetDirection(GetDirectionEvent event){
        EventBus.getDefault().removeStickyEvent(event);
        directionHelper.getDirections(event.getOrigin(),event.getDestination(),
                event.getLanguage(), event.getUnits(), event.getMode())
                .delay(3, TimeUnit.SECONDS).subscribe(e -> {
            EventBus.getDefault().postSticky(e);
        }, throwable -> {
            throwable.printStackTrace();
            EventBus.getDefault().postSticky(new ErrorEvent("Error during loading direction"));
        });
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) this
                    .getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            locationSubject.onNext(location);
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                locationSubject.onNext(location);
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(DealerService.this);
        }
    }

    /**
     * Function to get latitude
     */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("GRI", "onLocationChanged");
        locationSubject.onNext(location);
        stopUsingGPS();
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    class DealerBinder extends Binder {
        public DealerService getService() {
            return DealerService.this;
        }
    }
}
