package com.searchdealers.service;

import android.content.Intent;

/**
 * Created by danila on 16.07.16.
 */
public interface ServiceIntentProvider {
    Intent getIntentForService();
}
