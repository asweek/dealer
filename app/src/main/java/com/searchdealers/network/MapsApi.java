package com.searchdealers.network;

import com.searchdealers.model.maps.Direction;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by danila on 17.07.16.
 */
public interface MapsApi {
    @GET("json")
    Observable<Direction> getDirections(@Query("origin") String origin,
                                        @Query("destination") String destination,
                                        @Query("language") String language,
                                        @Query("units") String units,
                                        @Query("mode") String mode);
}
