package com.searchdealers.network;



import com.searchdealers.model.MegentoApi;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import rx.Observable;

/**
 * Created by danila on 16.07.16.
 */
public interface DealersApi {
    @Headers({
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
    })
    @GET("dealers")
    Observable<MegentoApi> getDealers();
}
