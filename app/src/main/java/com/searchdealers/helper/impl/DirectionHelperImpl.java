package com.searchdealers.helper.impl;

import com.searchdealers.helper.DirectionHelper;
import com.searchdealers.model.maps.Direction;
import com.searchdealers.network.MapsApi;
import com.searchdealers.util.RxUtil;

import rx.Observable;

/**
 * Created by danila on 17.07.16.
 */
public class DirectionHelperImpl implements DirectionHelper {

    private MapsApi repository;
    private RxUtil rxUtil;


    public DirectionHelperImpl(MapsApi repository, RxUtil rxUtil) {
        this.repository = repository;
        this.rxUtil = rxUtil;
    }


    @Override
    public Observable<Direction> getDirections(String origin, String destination,
                                               String language, String units,
                                               String mode) {
        return repository.getDirections(origin, destination, language, units, mode)
                .compose(rxUtil.applySchedulers());
    }
}
