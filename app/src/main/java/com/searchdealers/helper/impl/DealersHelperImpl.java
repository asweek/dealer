package com.searchdealers.helper.impl;

import com.searchdealers.helper.DealersHelper;
import com.searchdealers.model.MegentoApi;
import com.searchdealers.network.DealersApi;
import com.searchdealers.util.RxUtil;

import rx.Observable;

/**
 * Created by danila on 16.07.16.
 */
public class DealersHelperImpl implements DealersHelper {
    private DealersApi repository;
    private RxUtil rxUtil;


    public DealersHelperImpl(DealersApi repository, RxUtil rxUtil) {
        this.repository = repository;
        this.rxUtil = rxUtil;
    }

    public Observable<MegentoApi> getDealers() {
        return repository.getDealers()
                .compose(rxUtil.applySchedulers());
    }
}
