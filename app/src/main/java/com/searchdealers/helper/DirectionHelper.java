package com.searchdealers.helper;

import com.searchdealers.model.maps.Direction;

import rx.Observable;

/**
 * Created by danila on 17.07.16.
 */
public interface DirectionHelper {
    Observable<Direction> getDirections(String origin, String destination, String language,
                                        String units, String mode);
}
