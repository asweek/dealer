package com.searchdealers.helper;

import com.searchdealers.model.MegentoApi;

import rx.Observable;

/**
 * Created by danila on 16.07.16.
 */
public interface DealersHelper {
    Observable<MegentoApi> getDealers();
}
