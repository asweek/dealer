package com.searchdealers.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by danila on 16.07.16.
 */
@Root(name = "data_item", strict = false)
public class Dealer implements Parcelable {
    @Element(name = "address", required = false)
    public String address;
    @Element(name = "city", required = false)
    public String city;
    @Element(name = "website", required = false)
    public String website;
    @Element(name = "company", required = false)
    public String company;
    @Element(name = "latitude", required = false)
    public double latitude;
    @Element(name = "longitude", required = false)
    public double longitude;

    @Element(name = "phones", required = false)
    public Phones phones;

    @Element(name = "emails", required = false)
    public Emails emails;

    @Element(name = "goods", required = false)
    public Products products;

    public double distance;

    public Emails getEmails() {
        return emails;
    }

    public void setEmails(Emails emails) {
        this.emails = emails;
    }
    //    public Products products;
//
//    public Products getProducts() {
//        return products;
//    }
//
//    public void setProducts(Products products) {
//        this.products = products;
//    }

    public Phones getPhones() {
        return phones;
    }

    public void setPhones(Phones phones) {
        this.phones = phones;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getListProducts() {
        String list = "";
        if (products != null && products.getProductsList() != null) {
            for (int i = 0; i < products.getProductsList().size(); i++) {
                list = list + products.getProductsList().get(i);
                if (i != products.getProductsList().size() - 1) {
                    list = list + ", ";
                }
            }
        }
        return list;
    }

    public String getListPhones() {
        String list = "";
        if (phones != null && phones.getPhonesList() != null) {
            for (int i = 0; i < phones.getPhonesList().size(); i++) {
                list = list + phones.getPhonesList().get(i);
                if (i != phones.getPhonesList().size() - 1) {
                    list = list + ", ";
                }
            }
        }
        return list;
    }

    public String getDestination() {
        return String.valueOf(latitude) + "," + String.valueOf(longitude);
    }

    public Dealer() {
    }

    protected Dealer(Parcel in) {
        address = in.readString();
        city = in.readString();
        website = in.readString();
        company = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        phones = (Phones) in.readValue(Phones.class.getClassLoader());
        emails = (Emails) in.readValue(Emails.class.getClassLoader());
        products = (Products) in.readValue(Products.class.getClassLoader());
        distance = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(website);
        dest.writeString(company);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeValue(phones);
        dest.writeValue(emails);
        dest.writeValue(products);
        dest.writeDouble(distance);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Dealer> CREATOR = new Parcelable.Creator<Dealer>() {
        @Override
        public Dealer createFromParcel(Parcel in) {
            return new Dealer(in);
        }

        @Override
        public Dealer[] newArray(int size) {
            return new Dealer[size];
        }
    };
}
