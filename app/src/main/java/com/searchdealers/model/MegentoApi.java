package com.searchdealers.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by danila on 16.07.16.
 */
@Root(name = "magento_api",strict=false)
public class MegentoApi {
    @ElementList(name = "data_item", inline=true)
    private ArrayList<Dealer> dealers;

    public ArrayList<Dealer> getDealers ()
    {
        return dealers;
    }

    public void setData_item (ArrayList<Dealer> dealers)
    {
        this.dealers = dealers;
    }
}
