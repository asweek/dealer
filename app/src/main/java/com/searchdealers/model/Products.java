package com.searchdealers.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by danila on 17.07.16.
 */
@Root(name = "goods")
public class Products  implements Parcelable {
    @ElementList(entry = "data_item", inline=true, required = false)
    public ArrayList<String> productsList;

    public ArrayList<String> getProductsList() {
        return productsList;
    }

    public void setProductsList(ArrayList<String> productsList) {
        this.productsList = productsList;
    }

    protected Products(Parcel in) {
        if (in.readByte() == 0x01) {
            productsList = new ArrayList<String>();
            in.readList(productsList, String.class.getClassLoader());
        } else {
            productsList = null;
        }
    }

    public Products() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (productsList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(productsList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Products> CREATOR = new Parcelable.Creator<Products>() {
        @Override
        public Products createFromParcel(Parcel in) {
            return new Products(in);
        }

        @Override
        public Products[] newArray(int size) {
            return new Products[size];
        }
    };
}
