package com.searchdealers.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by danila on 17.07.16.
 */
@Root(name = "phones")
public class Phones implements Parcelable {
    @ElementList(entry = "data_item", inline=true)
    private ArrayList<String> phonesList;

    public ArrayList<String> getPhonesList() {
        return phonesList;
    }

    public void setPhonesList(ArrayList<String> phonesList) {
        this.phonesList = phonesList;
    }

    public Phones(){

    }

    protected Phones(Parcel in) {
        if (in.readByte() == 0x01) {
            phonesList = new ArrayList<String>();
            in.readList(phonesList, String.class.getClassLoader());
        } else {
            phonesList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (phonesList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(phonesList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Phones> CREATOR = new Parcelable.Creator<Phones>() {
        @Override
        public Phones createFromParcel(Parcel in) {
            return new Phones(in);
        }

        @Override
        public Phones[] newArray(int size) {
            return new Phones[size];
        }
    };
}
