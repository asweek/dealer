package com.searchdealers.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

/**
 * Created by danila on 17.07.16.
 */
@Root(name = "emails")
public class Emails implements Parcelable {
    @ElementList(entry = "data_item", inline=true)
    private ArrayList<String> emailsList;

    public ArrayList<String> getEmailsList() {
        return emailsList;
    }

    public void setEmailsList(ArrayList<String> emailsList) {
        this.emailsList = emailsList;
    }

    public Emails(){}

    protected Emails(Parcel in) {
        if (in.readByte() == 0x01) {
            emailsList = new ArrayList<String>();
            in.readList(emailsList, String.class.getClassLoader());
        } else {
            emailsList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (emailsList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(emailsList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Emails> CREATOR = new Parcelable.Creator<Emails>() {
        @Override
        public Emails createFromParcel(Parcel in) {
            return new Emails(in);
        }

        @Override
        public Emails[] newArray(int size) {
            return new Emails[size];
        }
    };
}