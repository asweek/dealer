package com.searchdealers.event;

/**
 * Created by danila on 16.07.16.
 */
public class ErrorEvent {
    private final String message;

    public ErrorEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
