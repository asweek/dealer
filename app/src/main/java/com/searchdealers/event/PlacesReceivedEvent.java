package com.searchdealers.event;

import android.location.Location;

import com.searchdealers.model.Dealer;
import com.searchdealers.model.MegentoApi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danila on 16.07.16.
 */
public class PlacesReceivedEvent {
    private MegentoApi data;
    private List<Dealer> dealersNearMe;
    private Location coordinates;

    public PlacesReceivedEvent(MegentoApi data, Location coordinates) {
        this.data = data;
        this.coordinates = coordinates;
    }

    public MegentoApi getData() {
        return data;
    }

    public Location getCoordinates() {
        return coordinates;
    }

    public void setData(MegentoApi data) {
        this.data = data;
    }

    public void setCoordinates(Location coordinates) {
        this.coordinates = coordinates;
    }

    public List<Dealer> getDealersNearMe() {
        return dealersNearMe;
    }

    public void setDealersNearMe(List<Dealer> dealersNearMe) {
        this.dealersNearMe = dealersNearMe;
    }
}
