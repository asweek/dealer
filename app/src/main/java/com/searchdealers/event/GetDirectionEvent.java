package com.searchdealers.event;

/**
 * Created by danila on 17.07.16.
 */
public class GetDirectionEvent {
    private String origin;
    private String destination;
    private String language;
    private String units;
    private String mode;

    public GetDirectionEvent(String origin, String destination, String language, String units,
                             String mode) {
        this.origin = origin;
        this.destination = destination;
        this.language = language;
        this.units = units;
        this.mode = mode;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
