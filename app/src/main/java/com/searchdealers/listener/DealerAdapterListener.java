package com.searchdealers.listener;

import com.searchdealers.model.Dealer;

/**
 * Created by danila on 17.07.16.
 */
public interface DealerAdapterListener {
    void onClickDealer(Dealer dealer);
}
