package com.searchdealers.di;

/**
 * Created by danila on 16.07.16.
 */
public interface HasComponent<T> {
    T getComponent();
}
