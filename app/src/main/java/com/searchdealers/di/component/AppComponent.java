package com.searchdealers.di.component;

import com.searchdealers.di.AppGraph;
import com.searchdealers.di.module.ApiModule;
import com.searchdealers.di.module.ApplicationModule;
import com.searchdealers.di.module.DaoModule;
import com.searchdealers.di.module.HelperModule;
import com.searchdealers.di.module.UtilModule;
import com.searchdealers.model.MegentoApi;

import javax.inject.Singleton;

import dagger.Component;
/**
 * Created by danila on 16.07.16.
 */
@Singleton
@Component(modules = {ApplicationModule.class,
        HelperModule.class,
        ApiModule.class,
        DaoModule.class,
        UtilModule.class})
public interface AppComponent extends AppGraph {

}
