package com.searchdealers.di.module;

import android.accounts.AccountManager;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.LocationManager;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
/**
 * Created by danila on 16.07.16.
 */
@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(Application application){
        this.application = application;
    }

    @Provides
    @Singleton
    public Application provideApplication(){
        return application;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext(){
        return application.getApplicationContext();
    }

    @Provides
    public Resources provideResources(){
        return application.getResources();
    }

    @Provides
    public AccountManager provideAccountManager(){
        return (AccountManager) application.getSystemService(Context.ACCOUNT_SERVICE);
    }

    @Provides
    public LocationManager provideLocationManager(){
        return (LocationManager) application.getSystemService(Context.LOCATION_SERVICE);
    }

    @Provides
    public SharedPreferences provideSharedPreferences(){
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Provides
    public ContentResolver provideContentResolver(){
        return application.getContentResolver();
    }
}
