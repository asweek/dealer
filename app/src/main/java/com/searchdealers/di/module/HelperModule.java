package com.searchdealers.di.module;

import com.searchdealers.helper.DealersHelper;
import com.searchdealers.helper.DirectionHelper;
import com.searchdealers.helper.impl.DealersHelperImpl;
import com.searchdealers.helper.impl.DirectionHelperImpl;
import com.searchdealers.network.DealersApi;
import com.searchdealers.network.MapsApi;
import com.searchdealers.util.RxUtil;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by danila on 16.07.16.
 */
@Module(includes = {ApiModule.class, UtilModule.class})
public class HelperModule {

    @Provides
    @Singleton
    public DealersHelper provideEalerHelper(DealersApi api, RxUtil rxUtil){
        return new DealersHelperImpl(api, rxUtil);
    }

    @Provides
    @Singleton
    public DirectionHelper provideDirectionHelper(MapsApi api, RxUtil rxUtil){
        return new DirectionHelperImpl(api, rxUtil);
    }
}
