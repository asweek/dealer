package com.searchdealers.di.module;

import com.searchdealers.network.DealersApi;
import com.searchdealers.network.MapsApi;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
/**
 * Created by danila on 16.07.16.
 */
@Module
public class ApiModule {
    private final static String BASE_URL = "http://mob.rockwool.oggettoweb.com/api/rest/";
    private final static String BASE_URL_DIRECTIONS =
            "http://maps.googleapis.com/maps/api/directions/";

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
        return client;
    }

    @Provides
    @Singleton
    @Inject
    @Named("apiClient")
    public Retrofit providedRetrofit(OkHttpClient okHttpClient){


        Strategy strategy = new AnnotationStrategy();

        Serializer serializer = new Persister(strategy);

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
                .build();
        return restAdapter;
    }

    @Provides
    @Singleton
    @Inject
    public DealersApi provideApiClient(@Named("apiClient") Retrofit adapter){
        return adapter.create(DealersApi.class);
    }

    @Provides
    @Singleton
    @Inject
    @Named("apiMapsClient")
    public Retrofit providedMapRetrofit(OkHttpClient okHttpClient){


        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(BASE_URL_DIRECTIONS)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        return restAdapter;
    }

    @Provides
    @Singleton
    @Inject
    public MapsApi provideMapApiClient(@Named("apiMapsClient") Retrofit adapter){
        return adapter.create(MapsApi.class);
    }

}
