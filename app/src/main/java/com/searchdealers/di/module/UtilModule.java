package com.searchdealers.di.module;

import com.searchdealers.util.DistanceUtil;
import com.searchdealers.util.RxUtil;
import com.searchdealers.util.impl.DistanceUtilImpl;
import com.searchdealers.util.impl.RxUtilImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by danila on 16.07.16.
 */
@Module
public class UtilModule {

    @Provides
    public RxUtil provideRxUtil(){
        return new RxUtilImpl();
    }

    @Provides
    public DistanceUtil provideDistanceUtil(){
        return new DistanceUtilImpl();
    }

}
