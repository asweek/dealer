package com.searchdealers.di;

import com.searchdealers.service.DealerService;
import com.searchdealers.ui.activity.MainActivity;
import com.searchdealers.ui.fragment.MapFragment;

public interface AppGraph {
    void inject(MainActivity a);
    void inject(MapFragment a);
    void inject(DealerService a);
}
