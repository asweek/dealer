package com.searchdealers;

import android.app.Application;
import android.content.Intent;

import com.searchdealers.di.HasComponent;
import com.searchdealers.di.component.AppComponent;
import com.searchdealers.di.component.DaggerAppComponent;
import com.searchdealers.di.module.ApplicationModule;
import com.searchdealers.service.DealerService;
import com.searchdealers.service.ServiceIntentProvider;

/**
 * Created by danila on 16.07.16.
 */
public class DealerApp extends Application implements HasComponent<AppComponent>, ServiceIntentProvider {

    private AppComponent mComponent;
    private Intent serviceIntent;

    @Override
    public void onCreate() {
        super.onCreate();

        initializeDependencies();
        initializeService();
    }

    private void initializeDependencies(){
        mComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    private void initializeService(){
        serviceIntent = new Intent(this, DealerService.class);
        startService(serviceIntent);
    }

    @Override
    public AppComponent getComponent() {
        return mComponent;
    }

    @Override
    public Intent getIntentForService() {
        return serviceIntent;
    }
}
