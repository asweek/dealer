package com.searchdealers.util.impl;


import com.searchdealers.util.RxUtil;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by user on 14.05.2016.
 */
public class RxUtilImpl implements RxUtil {
    @Override
    public <T> Observable.Transformer<T, T> applySchedulers() {
        return observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
