package com.searchdealers.util;

import rx.Observable;

/**
 * Created by user on 14.05.2016.
 */
public interface RxUtil {
    <T> Observable.Transformer<T, T> applySchedulers();
}
