package com.searchdealers.ui.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.searchdealers.R;
import com.searchdealers.event.GpsNotEnabledEvent;
import com.searchdealers.service.ServiceIntentProvider;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by danila on 16.07.16.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected void setupToolbar(android.support.v7.widget.Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public Fragment getFragmentByTag(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        injectDependencies();
    }

    @Subscribe(sticky = true)
    public void onGpsDisabledEvent(GpsNotEnabledEvent event){
        EventBus.getDefault().removeStickyEvent(event);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not required. Do you want to go enable it?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", (dialog, which) -> {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", (DialogInterface p1, int p2) ->{
            finish();
        });

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    protected void onStop() {
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backCount = fragmentManager.getBackStackEntryCount();

        if (backCount == 1) {
            finish();
        } else if (backCount > 1) {
            fragmentManager.popBackStack();
        } else {
            finish();
        }
    }

    protected abstract void injectDependencies();
}
