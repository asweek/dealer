package com.searchdealers.ui.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.searchdealers.R;
import com.searchdealers.di.HasComponent;
import com.searchdealers.di.component.AppComponent;
import com.searchdealers.event.BackPressedEvent;
import com.searchdealers.event.ErrorEvent;
import com.searchdealers.event.GetDirectionEvent;
import com.searchdealers.event.LoadPlacesEvent;
import com.searchdealers.event.PlacesReceivedEvent;
import com.searchdealers.helper.DealersHelper;
import com.searchdealers.model.Dealer;
import com.searchdealers.model.maps.Direction;
import com.searchdealers.service.ServiceIntentProvider;
import com.searchdealers.ui.fragment.BuildWayFragment;
import com.searchdealers.ui.fragment.ErrorFragment;
import com.searchdealers.ui.fragment.ListDealerFragment;
import com.searchdealers.ui.fragment.LoadingFragment;
import com.searchdealers.ui.fragment.MapFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    public static final String ARG_LOCATION = "arg_location";
    public static final String ARG_DEALER = "arg_dealer";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private ServiceConnection mServiceConn;

    @Inject
    public DealersHelper helper;

    private Dealer dealer;
    private Location coordinates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupToolbar(toolbar);
        mServiceConn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

            }
        };

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, LoadingFragment.newInstance(), LoadingFragment.TAG)
                    .commit();

            EventBus.getDefault().postSticky(new LoadPlacesEvent());
        } else {
            if(savedInstanceState.containsKey(ARG_LOCATION)) {
                coordinates = savedInstanceState.getParcelable(ARG_LOCATION);
                savedInstanceState.remove(ARG_LOCATION);
            }
            if(savedInstanceState.containsKey(ARG_DEALER)) {
                dealer = savedInstanceState.getParcelable(ARG_DEALER);
                savedInstanceState.remove(ARG_DEALER);
            }
        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(outState != null){
            outState.putParcelable(ARG_LOCATION, coordinates);
            outState.putParcelable(ARG_DEALER, dealer);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(mServiceConn);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(((ServiceIntentProvider) getApplicationContext()).getIntentForService(),
                mServiceConn,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(coordinates == null){
            EventBus.getDefault().postSticky(new LoadPlacesEvent());
        }
    }

    @Subscribe(sticky = true, priority = 100)
    public void onPlacesLoaded(PlacesReceivedEvent event) {
        coordinates = event.getCoordinates();

        Fragment loadingF = getSupportFragmentManager().findFragmentByTag(LoadingFragment.TAG);

        if(loadingF != null){
            getSupportFragmentManager().beginTransaction()
                    .remove(loadingF)
                    .commit();
        }

        MapFragment fragment = MapFragment.newInstance();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, MapFragment.TAG)
                .addToBackStack(MapFragment.TAG)
                .commit();
    }

    @Subscribe(sticky = true, priority = 100)
    public void onPlacesLoadError(ErrorEvent event) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, ErrorFragment.newInstance(), ErrorFragment.TAG)
                .addToBackStack(ErrorFragment.TAG)
                .commit();
    }

    @Subscribe(sticky = true)
    public void onShowDealerList(List<Dealer> event) {
        Fragment loadingFragment = getSupportFragmentManager()
                .findFragmentByTag(LoadingFragment.TAG);

        EventBus.getDefault().removeStickyEvent(event);
        ListDealerFragment fragment = ListDealerFragment.newInstance(event);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if(loadingFragment != null){
            ft.remove(loadingFragment);
        }
        ft.replace(R.id.fragment_container, fragment,
                        ListDealerFragment.TAG)
                .addToBackStack(ListDealerFragment.TAG)
                .commit();
    }

    @Subscribe(sticky = true)
    public void onDialerClick(Dealer event) {
        dealer = event;
        EventBus.getDefault().removeStickyEvent(event);

        Fragment loadingFragment = getSupportFragmentManager().findFragmentByTag(LoadingFragment.TAG);
        if(loadingFragment == null){
            loadingFragment = LoadingFragment.newInstance();
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, loadingFragment, LoadingFragment.TAG)
                .addToBackStack(LoadingFragment.TAG)
                .commit();

        EventBus.getDefault().postSticky(new GetDirectionEvent(getMyLocationDirection(),
                event.getDestination(), "ru", "metric", "driving"));
    }

    private String getMyLocationDirection() {
        if(coordinates != null){
        return String.valueOf(coordinates.getLatitude()) + ","
                + String.valueOf(coordinates.getLongitude());
        } else
            return "";

    }

    @Subscribe(sticky = true)
    public void onBuildWay(Direction event) {
        BuildWayFragment fragment = BuildWayFragment.newInstance(dealer, event);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, fragment, BuildWayFragment.TAG)
                .addToBackStack(BuildWayFragment.TAG)
                .commit();

        EventBus.getDefault().removeStickyEvent(event);
    }

    @Override
    protected void onDestroy() {
        if (!isChangingConfigurations()) {
            stopService(((ServiceIntentProvider) getApplicationContext()).getIntentForService());
        }
        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
               onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 1){
            EventBus.getDefault().post(new BackPressedEvent());
        }else {
            super.onBackPressed();
        }
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<AppComponent>) getApplicationContext())
                .getComponent().inject(this);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        // Inflate menu to add items to action bar if it is present.
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }
}
