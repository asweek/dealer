package com.searchdealers.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.searchdealers.R;
import com.searchdealers.listener.DealerAdapterListener;
import com.searchdealers.model.Dealer;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class DealerAdapter extends
        RecyclerView.Adapter<DealerAdapter.ViewHolder> {

    private List<Dealer> mValues;
    private DealerAdapterListener listener;

    public void setDealerValues(List<Dealer> items){
        mValues = items;
    }

    public DealerAdapter(List<Dealer> items, DealerAdapterListener listener) {
        mValues = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dealer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.tvCompany.setText(holder.mItem.company);
        holder.tvGoods.setText(holder.mItem.company);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_company)
        TextView tvCompany;
        @Bind(R.id.ll_item_dealer)
        LinearLayout llItemDaeler;
        @Bind(R.id.tv_goods)
        TextView tvGoods;
        public Dealer mItem;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            llItemDaeler.setOnClickListener(v -> listener.onClickDealer(mValues.get(getAdapterPosition())));
        }

    }
}
