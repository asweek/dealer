package com.searchdealers.ui.fragment;


import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.searchdealers.R;
import com.searchdealers.di.HasComponent;
import com.searchdealers.di.component.AppComponent;
import com.searchdealers.event.BackPressedEvent;
import com.searchdealers.event.PlacesReceivedEvent;
import com.searchdealers.model.Dealer;
import com.searchdealers.ui.Navigator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends BaseFragment implements OnMapReadyCallback {

    public static final String ARG_DEALER_NEAR_ME = "arg_dealer_near_me";

    private ArrayList<Dealer> dealersNearMe = new ArrayList<>();
    public static final String TAG = MapFragment.class.getSimpleName();

    private ViewHolder mViewHolder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewHolder = new ViewHolder(view);
        mViewHolder.mapView.onCreate(savedInstanceState);
        mViewHolder.mapView.getMapAsync(this);
        mViewHolder.tvDealersList.setOnClickListener(v -> EventBus.getDefault().postSticky(dealersNearMe));

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Дилеры");
        if (getArguments().containsKey(ARG_DEALER_NEAR_ME)) {
            dealersNearMe.clear();
            dealersNearMe.addAll(getArguments().getParcelableArrayList(ARG_DEALER_NEAR_ME));
        }
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<AppComponent>) getActivity().getApplicationContext())
                .getComponent().inject(this);
    }

    @Override
    public void onResume() {
        mViewHolder.mapView.onResume();
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onDestroy() {
        if(mViewHolder != null && mViewHolder.mapView != null) {
            mViewHolder.mapView.onDestroy();
        }
        super.onDestroy();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if(mViewHolder != null && mViewHolder.mapView != null) {
            mViewHolder.mapView.onLowMemory();
        }
    }

    public static MapFragment newInstance() {
        MapFragment f = new MapFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mViewHolder.map = googleMap;
        MapsInitializer.initialize(this.getActivity());
        mViewHolder.map.setMyLocationEnabled(true);
        for (Dealer d : dealersNearMe) {
            addPlaceToMap(mViewHolder.map, d);
        }
        if (!dealersNearMe.isEmpty()) {
            mViewHolder.tvCityName.setText(dealersNearMe.get(0).getCity());
            mViewHolder.tvCityName.setTypeface(null, Typeface.BOLD);
        }
//        mViewHolder.tvDealersList.setTypeface(Typeface.BOLD_ITALIC);
    }

    @Subscribe(sticky = true)
    public void onPlacesReceivedEvent(PlacesReceivedEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        dealersNearMe.clear();
        dealersNearMe.addAll(event.getDealersNearMe());
        getArguments().putParcelableArrayList(ARG_DEALER_NEAR_ME, dealersNearMe);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBackPressedMainThread(BackPressedEvent e){
        getActivity().finish();
    }

    private void addPlaceToMap(GoogleMap map, Dealer dealer) {
        LatLng position = new LatLng(dealer.getLatitude(), dealer.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions()
                .position(position)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .title(dealer.getCompany());
        map.addMarker(markerOptions);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public static final class ViewHolder {

        @Bind(R.id.mapview)
        public MapView mapView;
        @Bind(R.id.tv_city_name)
        TextView tvCityName;
        @Bind(R.id.tv_dealers_list)
        TextView tvDealersList;
        public GoogleMap map;

        public ViewHolder(View rootView) {
            ButterKnife.bind(this, rootView);
        }


    }
}