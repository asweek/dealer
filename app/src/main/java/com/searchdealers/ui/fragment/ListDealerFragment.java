package com.searchdealers.ui.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.searchdealers.R;
import com.searchdealers.event.BackPressedEvent;
import com.searchdealers.listener.DealerAdapterListener;
import com.searchdealers.model.Dealer;
import com.searchdealers.ui.Navigator;
import com.searchdealers.ui.adapter.DealerAdapter;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListDealerFragment extends Fragment implements DealerAdapterListener {

    public static final String ARG_DEALER_NEAR_ME = "arg_dealer_near_me";

    public static final String TAG = ListDealerFragment.class.getSimpleName();
    @Bind(R.id.list)
    RecyclerView list;
    @Bind(R.id.tv_by_alphabet)
    TextView tvByAlphabet;
    @Bind(R.id.tv_by_distance)
    TextView tvByDistance;
    private List<Dealer> dealersNearMe = new ArrayList<>();
    private DealerAdapter dealerAdapter;

    private boolean isSort = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listdealer_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        dealersNearMe = getArguments().getParcelableArrayList(ARG_DEALER_NEAR_ME);
        dealerAdapter = new DealerAdapter(dealersNearMe, this);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(dealerAdapter);
        list.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .color(ContextCompat.getColor(getActivity(), R.color.separator_color))
                .sizeResId(R.dimen.divider)
                .build());

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Список дилеров");
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @OnClick({R.id.tv_by_alphabet, R.id.tv_by_distance})
    public void onClick(View view) {
        isSort = !isSort;
        switch (view.getId()) {
            case R.id.tv_by_alphabet:
                Collections.sort(dealersNearMe, (object1, object2) -> {
                    if(isSort){
                        return object1.getCompany().toLowerCase()
                                .compareTo(object2.getCompany().toLowerCase());
                    } else {
                        return object2.getCompany().toLowerCase()
                                .compareTo(object1.getCompany().toLowerCase());
                    }
                });
                updateDateAdapter();
                break;
            case R.id.tv_by_distance:
                Collections.sort(dealersNearMe, (o1, o2) ->
                        Double.compare(o1.getDistance(), o2.getDistance()));
                updateDateAdapter();
                break;
        }
    }

    private void updateDateAdapter() {
        dealerAdapter.setDealerValues(dealersNearMe);
        dealerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClickDealer(Dealer dealer) {
        EventBus.getDefault().postSticky(dealer);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBackPressedMainThread(BackPressedEvent e){
        getActivity().getSupportFragmentManager().popBackStackImmediate(MapFragment.TAG, 0);
    }

    public static ListDealerFragment newInstance(List<Dealer> dealersNearMe) {
        ListDealerFragment fragment = new ListDealerFragment();
        Bundle args = new Bundle();
        ArrayList<Dealer> list = new ArrayList<Dealer>();
        list.addAll(dealersNearMe);
        args.putParcelableArrayList(ARG_DEALER_NEAR_ME, list);
        fragment.setArguments(args);
        return fragment;
    }

}
