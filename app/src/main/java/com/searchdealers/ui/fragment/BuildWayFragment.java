package com.searchdealers.ui.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.searchdealers.R;
import com.searchdealers.event.BackPressedEvent;
import com.searchdealers.model.Dealer;
import com.searchdealers.model.maps.Direction;
import com.searchdealers.model.maps.Leg;
import com.searchdealers.model.maps.Route;
import com.searchdealers.model.maps.Step;
import com.searchdealers.ui.Navigator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class BuildWayFragment extends BaseFragment implements OnMapReadyCallback {
    public static final String TAG = BuildWayFragment.class.getSimpleName();

    public static final String ARG_DEALER = "arg_dealer";
    public static final String ARG_DIRECTION = "arg_direction";

    private ViewHolder mViewHolder;
    private Dealer dealer;
    private Polyline newPolyline;
    private Direction direction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_build_way, container, false);
        return view;
    }


    @Override
    protected void injectDependencies() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mViewHolder.map = googleMap;
        MapsInitializer.initialize(this.getActivity());
        mViewHolder.map.setMyLocationEnabled(true);
        handleGetDirectionsResult();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mViewHolder = new ViewHolder(view);
        mViewHolder.mapWayView.onCreate(savedInstanceState);
        mViewHolder.mapWayView.getMapAsync(this);
        dealer = getArguments().getParcelable(ARG_DEALER);
        direction = getArguments().getParcelable(ARG_DIRECTION);
        if(dealer != null){
            mViewHolder.tvListProduction.setText(dealer.getListProducts());
            mViewHolder.tvListContact.setText(dealer.getListPhones());
            mViewHolder.tvCityName.setText(dealer.getCompany()+ " " + dealer.getCity());
        }
        if(direction != null){
            mViewHolder.tvCityDistance.setText(direction.getRoutes().get(0).getLegs().get(0)
                    .getDistance().getText());
        }


        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("");
//        Navigator.setup(this, toolbar);
    }
    public void handleGetDirectionsResult() {
        ArrayList<LatLng> lngs = new ArrayList<>();
        if(direction == null){
            return;
        }

        for(Route r : direction.getRoutes()){
            for(Leg l : r.getLegs()){
                for(Step step : l.getSteps()){
                    LatLng positionStart = new LatLng(step.getStartLocation().getLat(),
                            step.getStartLocation().getLng());
                    lngs.add(positionStart);
                    LatLng positionEnd = new LatLng(step.getEndLocation().getLat(),
                            step.getEndLocation().getLng());
                    lngs.add(positionEnd);
                }
            }
        }
//        int ii = 0;
//        for( ; ii <lngs.size() - 1; ii++) {
//            if(ii==0)
//            {
//                PolylineOptions rectLine = new PolylineOptions().width(8).color(Color.RED);
//                rectLine.add(lngs.get(ii));
//                Polyline polyline=mViewHolder.map.addPolyline(rectLine);
////                newPolyline.add(polyline);
//            }
//            else
//            {
//                PolylineOptions rectLine = new PolylineOptions().width(8).color(Color.RED);
//                rectLine.add(lngs.get(ii-1),lngs.get(ii));
//                Polyline polyline=mViewHolder.map.addPolyline(rectLine);
////                newPolyline.add(polyline);
//            }
//        }
//        PolylineOptions rectLine = new PolylineOptions().width(8).color(Color.RED);
//        rectLine.add(lngs.get(ii-1));
//        Polyline polyline=mViewHolder.map.addPolyline(rectLine);
//        polyline.add(polyline);
        //map.addPolyline(rectLine);

        PolylineOptions rectLine = new PolylineOptions();
        rectLine.addAll(lngs);
        rectLine.width(3);
        rectLine.color(Color.RED);
        if (newPolyline != null) {
            newPolyline.remove();
        }
        newPolyline =  mViewHolder.map.addPolyline(rectLine);
        if(dealer != null){
            LatLng position = new LatLng(dealer.getLatitude(), dealer.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(position)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                    .title(direction.getRoutes().get(0).getLegs().get(0).getEndAddress());
            mViewHolder.map.addMarker(markerOptions);
        }
    }

    @Override
    public void onResume() {
        mViewHolder.mapWayView.onResume();
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mViewHolder.mapWayView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mViewHolder.mapWayView.onLowMemory();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBackPressedMainThread(BackPressedEvent e){
        getActivity().getSupportFragmentManager().popBackStackImmediate(ListDealerFragment.TAG, 0);
    }

    public static final class ViewHolder {
        @Bind(R.id.tv_city_name)
        TextView tvCityName;
        @Bind(R.id.tv_city_distance)
        TextView tvCityDistance;
        @Bind(R.id.map_way_view)
        MapView mapWayView;
        @Bind(R.id.tv_production)
        TextView tvProduction;
        @Bind(R.id.tv_list_production)
        TextView tvListProduction;
        @Bind(R.id.tv_contacts)
        TextView tvContacts;
        @Bind(R.id.tv_list_contact)
        TextView tvListContact;

        public GoogleMap map;

        public ViewHolder(View rootView) {
            ButterKnife.bind(this, rootView);
        }

    }

    public static BuildWayFragment newInstance(Dealer dealer, Direction direction) {
        BuildWayFragment f = new BuildWayFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_DEALER, dealer);
        args.putParcelable(ARG_DIRECTION, direction);
        f.setArguments(args);
        return f;
    }
}
