package com.searchdealers.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.searchdealers.R;
import com.searchdealers.event.BackPressedEvent;
import com.searchdealers.event.ErrorEvent;
import com.searchdealers.event.PlacesReceivedEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ErrorFragment extends Fragment {

    private static final String ARG_ERROR_MESSAGE = "arg_error_message";

    public static final String TAG = ErrorFragment.class.getSimpleName();
    private  String errorMessage;
    @Bind(R.id.tv_error)
    TextView tvError;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_error, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        errorMessage = getArguments().getString(ARG_ERROR_MESSAGE, "Unexpected error occured");
        tvError.setText(errorMessage);
    }


    @Override
    public void onStart() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Subscribe(sticky = true)
    public void onReceivedError(ErrorEvent event){
        EventBus.getDefault().removeStickyEvent(event);
        errorMessage = event.getMessage();
        tvError.setText(errorMessage);
        getArguments().putString(ARG_ERROR_MESSAGE, errorMessage);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBackPressedMainThread(BackPressedEvent e){
        boolean isRouteVisible = getActivity().getSupportFragmentManager().findFragmentByTag(BuildWayFragment.TAG) != null;
        boolean isListVisible = getActivity().getSupportFragmentManager().findFragmentByTag(ListDealerFragment.TAG) != null;

        if(isRouteVisible){
            getActivity().getSupportFragmentManager().popBackStackImmediate(BuildWayFragment.TAG, 0);
        } else if(isListVisible){
            getActivity().getSupportFragmentManager().popBackStackImmediate(ListDealerFragment.TAG, 0);
        } else {
            getActivity().getSupportFragmentManager().popBackStackImmediate(MapFragment.TAG, 0);
        }
    }

    public static ErrorFragment newInstance() {
        ErrorFragment f = new ErrorFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }
}
